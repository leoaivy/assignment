import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SimpleLineChart from '../Chart/SimpleLineChart.jsx';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Send from '@material-ui/icons/Send';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import PriceTable from '../Table/Table.jsx'
import Hidden from '@material-ui/core/Hidden';
import api from '../../api'
import { env } from '../../utils'
import './Dashboard.scss'
import moment from 'moment'
import _ from 'lodash'

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  title: {
    flexGrow: 1,
    color: 'white'
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  tableContainer: {
    height: 320,
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
  button: {
    margin: theme.spacing.unit,
  }
});

class Dashboard extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
    this.callData = this.callData.bind(this)
    this.handleEnter = this.handleEnter.bind(this)
    this.handlePressTimeBtn = this.handlePressTimeBtn.bind(this)
    this.handlePriceChange = this.handlePriceChange.bind(this)
    this.updatePrice = this.updatePrice.bind(this)
    this.updatePrice = _.debounce(this.updatePrice, 300)
  }

  state = {
    tickerData: [
    ],
    ticker: '',
    error: {
      state: false,
      msg: ''
    },
    priceError: {
      state: false,
      msg: ''
    },
    calling: false,
    curBtn: 0,
    price: '',
    priceToSend: ''
  };

  handleChange (e) {
    this.setState({ 
      ticker: e.target.value,
      error: {
        state: false,
        msg: ''
      }
    })
  }

  handleEnter (e) {
    if(e.keyCode === 13) {
      this.handlePressTimeBtn(5)
    }
  }

  updatePrice () {
    this.setState({
      priceError: {
        state: false,
        msg: ''
      },
      priceToSend: this.state.price
    })
  }

  handlePriceChange (e) {
    this.setState({
      price: e.target.value
    }, () => {
      if (!this.state.price.length) {
        this.setState({
          priceError: {
            state: true,
            msg: 'You must type at least one character.'
          }
        })
        return
      } else if (isNaN(this.state.price)) {
        this.setState({
          priceError: {
            state: true,
            msg: 'You must type a number.'
          },
          priceToSend: ''
        })
        return
      }
      this.updatePrice()
    })
  }

  handlePressTimeBtn (numb) {
    this.setState({
      curBtn: numb
    })
    switch (numb) {
      case 1: // 1 week
        this.callData({
          start_date: moment().subtract(7, 'days').format('YYYY-MM-DD')
        })
        break
      case 2: // 1 month
        this.callData({
          start_date: moment().subtract(1, 'months').format('YYYY-MM-DD')
        })
        break
      case 3: // 3 months
        this.callData({
          start_date: moment().subtract(3, 'months').format('YYYY-MM-DD')
        })
        break
      case 4: // 6 months
        this.callData({
          start_date: moment().subtract(6, 'months').format('YYYY-MM-DD')
        })
        break
      case 5: // 1 years
        this.callData({
          start_date: moment().subtract(12, 'months').format('YYYY-MM-DD')
        })
        break
      case 6: // 1 years
        this.callData({
          start_date: moment().subtract(5, 'years').format('YYYY-MM-DD')
        })
        break
      case 7: // 1 years
        this.callData()
        break
      default:
        break
    }
  }

  callData (opt = {}) {
    if (!this.state.ticker.length) {
      this.setState({
        error: {
          state: true,
          msg: 'Please type at least one character.'
        }
      })
      return
    } else if (this.state.ticker[0] === '/') {
      this.setState({
        error: {
          state: true,
          msg: 'You should not start with "/"'
        }
      })
      return
    }
    // Empty data and open loading before calling
    this.setState({
      tickerData: [],
      calling: true,
      priceError: {
        state: false,
        msg: ''
      }
    })
    // Calling data
    api.get({
      url: env.API.GET_LIST_STICKERS + this.state.ticker + '/data.json',
      data: {
        column_index: 4,
        exclude_column_names: true,
        order: 'asc',
        collapse: 'daily',
        api_key: 'tpdr8AueWey_xTsnWhi5',
        ...opt
      }
    }).then(res => {
      let tickerData = res.data.dataset_data.data.map(itm => ({
        'Date': itm[0],
        'Close Price': itm[1]
      }))
      this.setState({
        error: {
          state: false,
          msg: ''
        },
        tickerData,
        calling: false
      })
    }).catch(error => {
      this.setState({
        error: {
          state: true,
          msg: error.response.data.quandl_error.message
        },
        calling: false
      })
    })
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <CssBaseline />
        <AppBar
          position="absolute"
          className={classNames(classes.appBar)}
        >
          <Toolbar className={classes.toolbar}>
            <Typography
              component="h1"
              variant="h6"
              noWrap
              className={classes.title}
            >
              Sentifi Stock Chart
            </Typography>
          </Toolbar>
        </AppBar>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Hidden mdDown>
            <div className={classes.appBarSpacer} />
          </Hidden>
          <Grid container spacing={24} className='app-container'>
            <Grid item xs={12} md={6}>
              <TextField
                error={this.state.error.state}
                id="standard-name"
                label="Ticker"
                placeholder="Press Enter to Search"
                margin="normal"
                helperText={this.state.error.msg}
                fullWidth
                value={this.state.ticker}
                onKeyUp={this.handleEnter}
                onChange={this.handleChange}
                InputProps={{
                  endAdornment: 
                  <InputAdornment position="start">
                    <IconButton
                      onClick={this.handlePressTimeBtn.bind(this, 5)}
                      color="primary"
                    >
                      <Send />
                    </IconButton>
                  </InputAdornment>
                }}
              />
            </Grid>
            {/* END: TICKER SYMBOL INPUT */}
            <Grid item xs={12} md={6}>
              <TextField
                error={this.state.priceError.state}
                id="standard-name"
                label="Close Price"
                placeholder="Type a Price"
                margin="normal"
                helperText={this.state.priceError.msg}
                value={this.state.price}
                onChange={this.handlePriceChange}
                fullWidth
              />
            </Grid>
            {/* END: CLOSE PRICE INPUT */}
            <Grid item xs={12} md={9}>
              <div className="chart">
                <div className="chart__heading">
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 1 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 1)}
                    className={this.state.curBtn === 1 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    1 week
                  </Button>
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 2 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 2)}
                    className={this.state.curBtn === 2 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    1 month
                  </Button>
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 3 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 3)}
                    className={this.state.curBtn === 3 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    3 months
                  </Button>
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 4 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 4)}
                    className={this.state.curBtn === 4 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    6 months
                  </Button>
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 5 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 5)}
                    className={this.state.curBtn === 5 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    1 year
                  </Button>
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 6 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 6)}
                    className={this.state.curBtn === 6 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    5 years
                  </Button>
                  <Button 
                    variant="contained" 
                    color={this.state.curBtn === 7 ? 'primary': 'default'}
                    onClick={this.handlePressTimeBtn.bind(this, 7)}
                    className={this.state.curBtn === 7 ? 'chart__btn--active chart__btn' : 'chart__btn'}
                  >
                    All
                  </Button>
                </div>
                {/* END: TIME RANGE BUTTONS */}
                {this.state.calling &&
                  <div className="text-center">
                    <CircularProgress color="secondary" />
                  </div>
                }
                {/* END: LOADING ICON */}
                {!this.state.calling && !this.state.error.state && this.state.tickerData.length > 0 &&
                  <Typography component="div" className="chart__wrap">
                    <SimpleLineChart tickerData={this.state.tickerData} />
                  </Typography>
                }
                {/* END: CHART */}
              </div>
            </Grid>
            <Grid item xs={12} md={3}>
              {this.state.tickerData.length > 0 &&
                <PriceTable data={this.state.tickerData} price={this.state.priceToSend} />
              }
            </Grid>
            {/* END: CLOSE PRICE TABLE */}
          </Grid>
        </main>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Dashboard);