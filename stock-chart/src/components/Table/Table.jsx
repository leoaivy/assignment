import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './Table.scss'

const PriceTable = (props) => {
    let main_data = props.data.filter(itm => itm['Close Price'] > parseInt(props.price))
    if(!props.price.length || !main_data.length) {
        return (
            <Paper className="chart__table chart__table--empty">
                No values.
            </Paper>
        )
    }

    return (
        <Paper className="chart__table">
            <Table>
                <TableHead>
                <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell align="right">Close Price</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                    {main_data.map((itm, id) => (
                        <TableRow key={id}>
                            <TableCell component="th" scope="row">
                                {itm.Date}
                            </TableCell>
                            <TableCell align="right">
                                {itm['Close Price']}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    )
}

export default PriceTable