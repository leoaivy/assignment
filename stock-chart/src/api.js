// import Vue from 'vue'
import axios from 'axios'
// import router from '@/router'
import { env } from './utils'

let config = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
}
let _generateParams = (obj) => {
  return Object.keys(obj).map((key) => {
    return key + '=' + encodeURIComponent(obj[key])
  }).join('&')
}
let _combineHeader = (headers) => {
  let obj = Object.assign(config, {})
  if (headers) {
    for (let p in headers) {
      obj.headers[p] = headers[p]
    }
  }
  return obj
}
export default {
  install () {
    axios.defaults.baseURL = env.API_SERVER
    // axios.defaults.withCredentials = true
    for (let name in config.headers) {
      axios.defaults.headers.common[name] = config.headers[name]
    }
    // In 30 second
    axios.defaults.timeout = 30000

    axios.interceptors.request.use((config) => {
      return config
    })

    // axios.interceptors.response.use((config) => {
    //   if (config && config.data && config.data.StatusCode !== 200 && config.data.IsRedirect) {
    //     if (config.config.url.indexOf('api.myjson.com') !== -1) {
    //       return config
    //     }
    //     router.push({ name: 'error', params: { id: config.data.StatusCode } })
    //   } else {
    //     return config
    //   }
    // }, (error) => {
    //   if (error.response && error.response.status !== 200 && error.response.status !== 404) {
    //     if (error.response.status === 401 || error.response.status === 403 || error.response.status === 500) {
    //       const ErrorComponent = Vue.extend(Error)
    //       new ErrorComponent({
    //         propsData: {
    //           errorCode: error.response.status
    //         }
    //       }).$mount('#app')
    //     } else {
    //       router.push({ name: 'error', params: { id: error.response.status } })
    //     }
    //   }
    //   if (error.response && error.response.status === 404) {
    //     error.response.data = []
    //   }
    //   return error.response
    // })
  },
  post: (obj) => {
    return axios.post(obj.url, obj.data, _combineHeader(obj.headers))
  },
  get: (obj) => {
    return axios.get(obj.url + (obj.data ? ('?' + _generateParams(obj.data)) : ''), _combineHeader(obj.headers))
  },
  put: (obj) => {
    return axios.put(obj.url, JSON.stringify(obj.data), _combineHeader(obj.headers))
  }
}
