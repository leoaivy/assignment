const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_SERVER: 'https://www.quandl.com/api/v3/datasets/WIKI',
  API: {
  }
})
