let env = {}
if (process.env.NODE_ENV === 'production') { env = require('./config/prod.env') }
if (process.env.NODE_ENV === 'development') { env = require('./config/dev.env') }

module.exports = {
  env: env
}
