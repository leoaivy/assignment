import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './views/Home.jsx'
import api from './api'
import './App.scss'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

api.install()

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#f57b20'
    },
    secondary: {
      main: '#f57b20',
    },
  },
});

function App () {
  return (
    <Router>
      <MuiThemeProvider theme={theme}>
        <div>
          <Route exact path="/" component={Home} />
        </div>
      </MuiThemeProvider>
    </Router>
  );
}

export default App