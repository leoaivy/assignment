import React from 'react'
import Dashboard from '../components/Dashboard/Dashboard.jsx'

function Home() {
    return (
      <div>
        <Dashboard />
      </div>
    );
}

export default Home